| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 4 | Software & Control System |

| Agenda |
| ----------- |
| Extracting more understanding about the HAU research paper |
| Add more contents within GITLAB |

| Goals |
| ----------- |
| Able to specifically understand the fundamentals behind control system of an Airship(HAU) |
| Contents such as HAU Research Paper, and Relevant images should be added |

| Method & Justification |
| ----------- |
| Attended a meeting with Azizi on thursday |
| A repository for CS is added, whereas the contents are added |

| Impact on the project |
| ----------- |
| The meeting itself gave an overview, and a better understanding with regards to the relationship of the flight dynamics of HAU with CS |

| Next step |
| ----------- |
| Get an overview on how to calibrate a quadcopter(conceptually the same as HAU) |
