| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 6 | Software & Control System |

| Agenda |
| ----------- |
| Refurbishing Subsystem's GITLAB Repo |
| Undestand other components that will be implemented |

| Goals |
| ----------- |
| Add more information with regards to the control system of the HAU in GITLAB |
| Implementation of sprayer in the control system |

| Method & Justification |
| ----------- |
| Topics of CS of HAU are listed in an organized manner |
| Controls of the sprayer is implemented within the Remote Controller, and transmitted via the transmitter in a "on and off switch" manner |

| Impact on the project |
| ----------- |
| Informative topics are crucial so that the other subsystem will be able to align anything accordingly |
| The sprayer team will be informed for this matter |

| Next step |
| ----------- |
| Firmware/Operating system of the HAU  |
