| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 2 | Software & Control System |

| Agenda |
| ----------- |
| Introduction to HAU |
| Introduction Gitlab Repository and Markdowns |

| Goals |
| ----------- |
| Understanding the Subsystems and its importance |
| Learn how to access, and do markdowns in Gitlab |

| Method & Justification |
| ----------- |
| Filled in a survey whereas it will determine each individual's subsystem |
| Markdowns are edited via Web-browser's IDE and Visual Studio Code |

| Impact on the project |
| ----------- |
| Able to understand one's true self,characteristics and attitude |

| Next step |
| ----------- |
| To begin Familiarizing simulink, along with preparing for next week's task |
