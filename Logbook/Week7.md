| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 8 | Software & Control System |

| Agenda |
| ----------- |
| Investigating the Compiling program  |
| Pre-installing Ubuntu, it's libraries and tools  |


| Goals |
| ----------- |
| Compiling body of codes into 1 program format(firmware for the airship) |

| Method & Justification |
| ----------- |
| Configuration of ArduPilot in Linux environment to begin coding |
| Installation of Pre-requisites for the Linux environment |

| Impact on the project |
| ----------- |
| Error in the code meant that there is a need to modify certain functions |
| Some libraries was ahead of time, thus it is must that it be downgraded |
| Difficulties in pinpointing the actual cause of the error |

| Next step |
| ----------- |
|  Full compilation of the firmware |
