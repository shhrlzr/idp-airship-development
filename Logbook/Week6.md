| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 7 | Software & Control System |

| Agenda |
| ----------- |
| Investigate the firmware/operating system of the HAU  |


| Goals |
| ----------- |
| Looking for a way whereas the code can be modified to suit with the given conditions, and be implemented in the controller of the HAU |

| Method & Justification |
| ----------- |
| Compiling and uploading the codes to the controller |

| Impact on the project |
| ----------- |
| Yet to be done as the muchly needed equipments and components are yet to arrive. |

| Next step |
| ----------- |
|  The controls for the sprayer |
