| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 9 | Software & Control System |

| Agenda |
| ----------- |
| Pre-building a quadcopter |
| Solving the error yielded from last week's  |


| Goals |
| ----------- |
| Better understanding the behaviour of a quadcopter before the main HAU |
| Investigating the problem faced after uploading a pre-built firmware of quadcopter |
| Getting rid of the compiling error |

| Method & Justification |
| ----------- |
| Assembled key components of the quadcopter |
| Ran a test to see the problems when starting up the quadcopter |
| Researched various ways to remove the error, and ran a "trial&error" method |

| Impact on the project |
| ----------- |
| The problem of the quadcopter persisted, mainly around the transmitter |
| Downgraded some of the libraries to get the compilation process up and running |
| The ArduHAU firmware (Azizi Malek's) is outdated, thus it a must that a new firmware be produced |

| Next step |
| ----------- |
| Compilation of the latest firmware |
| Initiation of developing the sprayer system |

