| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 3 | Software & Control System |

| Agenda |
| ----------- |
| Construct a gantt chart for our own Subsystem |

| Goals |
| ----------- |
| Estimating the progress duration of each tasks |

| Method & Justification |
| ----------- |
| All subsystem propose their duration of task in a unified gantt chart |

| Impact on the project |
| ----------- |
| The alignment of tasks are adjusted according to the provided gantt chart |

| Next step |
| ----------- |
| To begin studying the fundamentals and concept of the Airship |

