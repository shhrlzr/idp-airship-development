| Name | No. Matric | Week | Subsystems |
| ----------- | ----------- | ----------- | ----------- |
| Mohd Shahrul Azri Bin Jiprin | 195934 | 5 | Software & Control System |

| Agenda |
| ----------- |
| Calibration of Quadcopter |
| Regain understand for other group members |

| Goals |
| ----------- |
| Understand the calibration methods of yaw,roll, and pitch behaviour |
| Understand the radio calibration(remote control) |
| Understand the compass calibration |
| Able to explain everything to the other group members |

| Method & Justification |
| ----------- |
| Mission planner is used to calibrate accelerometer,radio, and compass calibration |
| A brief explaination was conducted with each members individually |

| Impact on the project |
| ----------- |
| Calibration is muchly needed to ensure that there are no errors upon controlling each components of the HAU |
| The group members can hopefully understand the overall progress of this subsystem |

| Next step |
| ----------- |
| Transmission of sprayer signal  |
